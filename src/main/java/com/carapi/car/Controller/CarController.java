package com.carapi.car.Controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.carapi.car.Model.Car;
import com.carapi.car.Model.CarType;
import com.carapi.car.Service.CarService;
import com.carapi.car.Service.CarTypeService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CarController {
    @Autowired
    private CarService carService;

    @Autowired
    private CarTypeService carTypeService;

    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAllCars(){
        try {
            return new ResponseEntity<>(carService.getAllCars(),HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/cartypes")
    public ResponseEntity<Set<CarType>> getCarTypeByCarCode(@RequestParam(value = "carCode") String carCode){
        try {
            Set<CarType> cartypes = carService.getCarTypeByCarCode(carCode);
            if (cartypes != null) {
               return new ResponseEntity<>(cartypes, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/cartypes-all")
    public ResponseEntity<List<CarType>> getAllCarType(){
        try {
            return new ResponseEntity<>(carTypeService.getAllCarTypes(),HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}