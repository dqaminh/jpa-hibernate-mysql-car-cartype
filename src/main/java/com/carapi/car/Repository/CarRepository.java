package com.carapi.car.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carapi.car.Model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {
   Car findByCarCode(String carCode);
}
