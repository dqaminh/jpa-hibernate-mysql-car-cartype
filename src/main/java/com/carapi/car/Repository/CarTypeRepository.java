package com.carapi.car.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carapi.car.Model.CarType;

public interface CarTypeRepository extends JpaRepository<CarType , Long> {
    
}
