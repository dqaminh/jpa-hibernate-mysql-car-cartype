package com.carapi.car.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carapi.car.Model.CarType;
import com.carapi.car.Repository.CarTypeRepository;

@Service
public class CarTypeService {
    @Autowired
    CarTypeRepository iCarTypeRepository;

    public ArrayList<CarType> getAllCarTypes(){
        ArrayList<CarType> lCarTypes = new ArrayList<CarType>();

        iCarTypeRepository.findAll().forEach(lCarTypes::add);

        return lCarTypes;
    }
}
