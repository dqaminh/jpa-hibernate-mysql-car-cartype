package com.carapi.car.Service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carapi.car.Model.Car;
import com.carapi.car.Model.CarType;
import com.carapi.car.Repository.CarRepository;

@Service
public class CarService {
    @Autowired
    CarRepository iCarRepository;

    public ArrayList<Car> getAllCars(){
        ArrayList<Car> lCars = new ArrayList<>();

        iCarRepository.findAll().forEach(lCars::add);

        return lCars;
    }

    public Set<CarType> getCarTypeByCarCode(String carCode){
        Car vCar = iCarRepository.findByCarCode(carCode);
        if (vCar != null) {
            return vCar.getTypes();
        } else {
            return null;
        }
    }
    
}
